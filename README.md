# WSUS Downloader
Application web pour télécharger des mises à jour WSUS à la manière du catalogue WSUS sur internet, mais sur un environnement isolé.

## Schéma de principe
TODO

## Technologies utilisées
Frontend Python/Flask avec une base SQL pour le lising des mises à jour, et SQLite pour la configuration de l'application (numéro de version, URL du serveur...).

## Interface du portail
TODO

## Données issues du serveur WSUS
### Connexion à la base de données intégrée à Windows (WID)

Malheureusement, la connexion ne se fait que localement...

Avec Microsoft SQL Server Management Stduio : utiliser la chaîne de connexion `np:\\.\pipe\MICROSOFT##WID\tsql\query`

En powershell :
```powershell
$connectionstring = "server=\\.\pipe\MICROSOFT##WID\tsql\query;database=SUSDB;trusted_connection=true;"
$SQLConnection= New-Object System.Data.SQLClient.SQLConnection($ConnectionString)
$SQLConnection.Open()
$SQLCommand = $SQLConnection.CreateCommand()
$SQLCommand.CommandText = 'SELECT TOP (1000) [LocalUpdateID],[UpdateID],[UpdateTypeID] FROM [SUSDB].[dbo].[tbUpdate]'
$SqlDataReader = $SQLCommand.ExecuteReader()
$SQLDataResult = New-Object System.Data.DataTable
$SQLDataResult.Load($SqlDataReader)
$SQLConnection.Close()
$SQLDataResult
```

### Procédures stockées
La base contient un tas de procédures stockées, à voir comment les lancer en PowerShell.

Par exemple :
```sql
USE [SUSDB]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[spGetExportData]

SELECT	'Return Value' = @return_value

GO
```
On récupère toutes les mises à jour

#### Lien entre l'ID de mise à jour et le hash du fichier
Voir la procédure stockée spGetAllUpdateUrls

### Emplacement des fichiers

Le fichier porte comme nom sa signature SHA1 que l'on peut récupérer par exemple avec une commande PowerShell :

```powershell
Get-FileHash .\42C6F69FE36624880CBA5292C3962E3F08993390.exe -algorithm SHA1 |fl

Algorithm : SHA1
Hash      : 42C6F69FE36624880CBA5292C3962E3F08993390
Path      : E:\WsusContent\90\42C6F69FE36624880CBA5292C3962E3F08993390.exe
```

Le dossier dans lequel se trouve ce fichier a comme nom les deux derniers caracteres du checksum SHA1
